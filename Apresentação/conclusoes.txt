Papel das Disciplinas

	* Administração Geral: a disciplina ajudou a enxergar a estrutura da empresa a ser montada, ajudou na elaboração do plano de negócio, ajudou na identificação da viabilidade do negócio verificando as ameaças e oportunidades.
	* Algoritmos e Lógica de Programação: a disciplina ajudou a entender a desenvolver a lógica para realizar os cálculos da folha de pagamento dos funcionários, possibilitou também ver a aplicação prática da elaboração de algoritmos.
	* Arquitetura e Organização de Computadores: a disciplina ajudou na elaboração do fluxograma de rede utilizada no negócio, assim como as especificações dos equipamentos e demais recursos a serem utilizados, ajudou também na compreensão da estrutura organizacional de todo equipamento previsto no projeto.
	* Programação em Microinformática: a disciplina ajudou no desenvolvimento de sistema simples de gestão do negócio, forneceu ferramentas para uma melhor organização, análise de desempenho e estratégia de marketing.
	* Laboratório de Hardware: a disciplina ajudou no conhecimento necessário para a montagem da estrutura da tecnologia do projeto, além de fornecer ferramentas para gerar um plano de manutenção corretiva e preventiva.
	* Inglês: 

LIÇÕES APRENDIDAS E PRINCIPAIS DIFICULDADES
Aprendemos a estruturar e organizar o projeto, noções administrativas, a importância de planos preventivos na parte administrativa e de hardware, ajudou a visualizar forças e fraquezas no projeto, colaborou no desenvolvimento da lógica para identificar problemas e encontrar as soluções, incentivou a criação de formas de organização em ferramentas de gestão. Tivemos dificuldade devido a redundância no desenvolvimento de ferramentas distintas para administração do negócio.

OPORTUNIDADES DE MELHORIA
Infelizmente faltou a parte de pesquisa para um melhor desenvolvimento do projeto, não foi possível focar bem a estratégia administrativa e ocorreu um desalinhamento entre o projeto e curso. Para melhoria temos em mente um sistema de acompanhamento de desempenho dos alunos da academia, pesquisa orçamentaria mais aprofunda.

CONCLUSÕES
Conseguimos atender todos os requisitos do edital, chegando avançar em alguns pontos como a parte de gestão financeira. Nos ajudou a identificar a importância do sistema de informação para uma boa gestão do negócio. Para a nossa área é importante o conhecimento de gestão de negócios e a identificação de oportunidades existentes no mercado, mesmo com diversas ferramentas no sempre é necessário desenvolver ou aprimorar alguma metodologia.
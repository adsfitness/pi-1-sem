Início 

Escopo: https://bitbucket.org/adsfitness/pi-1-sem/commits/7b166a44e9e3664c4c3db65ece34912ce17f88a3

**Administração Geral:**

Rascunho ->  https://bitbucket.org/adsfitness/pi-1-sem/commits/edced2d7aae42164c62b6642028bf1084eabb5d5

Entrega 19 a 23/10-> Modelo de negócios, principais processos mapeados e tipo de estrutura

**Programação em microinformática:**

Entrega 19 a 23/10-> Banco de dados em Access do projeto. **AULA DIA 29/09**

**Algoritmos**

Entrega 19 a 23/10 -> Entrega parcial do projeto de Folha de Pagamento

**Matemática Discreta**

16 a 20/11 -> Entrega da versão final da matriz. **Utilizar a matriz na folha de pagamento**

**Inglês**

Entrega 19 a 23/10 -> Entrega do corpo do trabalho, com a descrição da empresa e seu produto ou serviço e do vídeo. 
**Entrega final: Elaborar roteiro, em inglês. Fazer as legendas em português. **

**Arquitetura e Organização de Computadores**

Entrega 19 a 23/10 -> Modelo de negócios, principais processos mapeados e tipo de estrutura

**Laboratório de Hardware**

Entrega 19 a 23/10 -> Definição e valores de Hardwares/Softwares e serviços de manutenção e suporte